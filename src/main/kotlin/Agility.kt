/**
 * @author sigmotoa
 *
 * This class contains some popular brain games with numbers
 */


class Agility {


    //bigger than

    //Show if the first number is bigger than the second

    fun biggerThan(numA: String, numB:String):Boolean
    {
        var result:Boolean = false
        result = numA.toDouble() > numB.toDouble()
        return result
    }


    //Sort from bigger the numbers an show in list

    fun order(numA: Int, numB: Int, numC: Int, numD: Int, numE: Int): List<Int?> {
        return listOf(numA, numB, numC, numD, numE).sorted()
    }

    //Look for the smaller number of the list
    fun smallerThan(list: List<Double>): Double{
        var result:Double = 0.0
        var listorder = list.sorted()
        result = listorder[0]
        return result
    }
    //Palindrome number is called in Spanish capicúa
    //The number is palindrome

    fun palindromeNumber(numA: Int): Boolean
    {
        var result:Boolean = false
        var (igual, aux) = Pair(0, 0)
        var texto: String = numA.toString()
        for (ind in texto.length - 1 downTo 0) {
            when { texto[ind] == texto[aux] -> igual++ }
            aux++
        }
        result = when (igual) {
            texto.length -> true
            else -> false
        }
        return result
    }
    //the word is palindrome?
    fun palindromeWord(word: String): Boolean
    {
        var result:Boolean = false
        var (igual, aux) = Pair(0, 0)
        var texto: String = word.toString()
        for (ind in texto.length - 1 downTo 0) {
            when { texto[ind] == texto[aux] -> igual++ }
            aux++
        }
        result = when (igual) {
            texto.length -> true
            else -> false
        }
        return result
    }

    //Show the factorial number for the parameter
    fun factorial(numA: Int):Int
    {
        var factorial: Int = 1
        for (i in 1..numA) {
            // factorial = factorial * i;
            factorial *= i.toInt()
        }
        return factorial
    }

    //is the number odd
    fun is_Odd(numA: Byte): Boolean {
        return numA % 2 != 0
    }

    //is the number prime
    fun isPrimeNumber(numA:Int): Boolean
    {
        var result:Boolean = false
        var Cont = 0
        var Lim = 0
        (1 .. numA!!).forEach { div ->
            if(numA % div == 0) {
                Cont++
                Lim++
                if(Lim == 10) {
                    Lim = 0
                }
            }
        }
        result = when(Cont) {
            2 -> true
            else -> false
        }
        return result
    }

    //is the number even
    fun is_Even(numA: Byte): Boolean
    {
        return numA % 2 == 0
    }
    //is the number perfect
    fun isPerfectNumber(numA:Int): Boolean
    {
        var suma = 0
        for(i in 1 .. numA / 2) {
            if(numA % i == 0) {
                suma += i
            }
        }
        if(suma == numA) { return true }
        return false
    }
    //Return an array with the fibonacci sequence for the requested number
    fun fibonacci(numA: Int): List<Int?>
    {
        var fib = 0
        var aux = 1
        val fibonacci = mutableListOf(0)
        if(numA > 0) {
            (0 .. numA).forEach { fibonacci.add(fib)
                aux += fib
                fib = aux - fib
            }
        } else {
            println("El numero debe ser mayor a cero!!")
        }
        fibonacci.removeAt(0)
        return fibonacci
    }
    //how many times the number is divided by 3
    fun timesDividedByThree(numA: Int):Int
    {
        var num = numA / 3
        num = num.toInt()
        return num
    }

    //The game of fizzbuzz
    fun fizzBuzz(numA: Int):String?
    {
        var result: String = ""
        /**
         * If number is divided by 3, show fizz
         * If number is divided by 5, show buzz
         * If number is divided by 3 and 5, show fizzbuzz
         * in other cases, show the number
         */
        if ( numA%15 == 0) {
            result = "FizzBuzz"
        }
        else if(numA%5 == 0) {
            result = "Buzz"}
        else if(numA%3 == 0){
            result = "Fizz"}
        else {
            result = numA.toString()
        }
        return result
    }

}

